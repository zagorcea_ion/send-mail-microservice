package entity

import (
	"database/sql/driver"
	"encoding/json"
)

//The main Mail entity structure
type Mail struct {
	Id        int      `json:"id" db:"id"`
	Subject   string   `json:"subject" db:"subject" binding:"required"`
	Content   string   `json:"content" db:"content" binding:"required"`
	UserID    int      `json:"user_id" db:"user_id"`
	Status    int      `json:"status" db:"status"`
	Emails    Emails   `json:"emails" db:"emails" binding:"required"`
	CreatedAt string   `json:"created_at" db:"created_at"`
}

//This structure is a part of main Mail entity structure. That represent Emails field.
type Emails []string

//Make the Emails struct implement the driver.Valuer interface. This method
//simply returns the JSON-encoded representation of the struct.
func (e Emails) Value() (driver.Value, error) {
    if len(e) == 0 {
        return "[]", nil
    }
    return json.Marshal(e)
}

//Make the Emails struct implement the sql.Scanner interface. This method
//simply decodes a JSON-encoded value into the struct fields.
func (e *Emails) Scan(src interface{}) (err error) {
    var emails []string

    switch src.(type) {
    case string:
        err = json.Unmarshal([]byte(src.(string)), &emails)

    case []byte:
        err = json.Unmarshal(src.([]byte), &emails)
    }

    if err != nil {
        return err
    }

    *e = emails

    return nil
}

//Get all mails collection structure
type GetAllMailsResponse struct {
	Data []Mail `json:"data"`
}