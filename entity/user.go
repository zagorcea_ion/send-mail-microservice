package entity

//The main User entity structure
type User struct {
	Id 		 	 int    `json:"-" db:"id"`
	Name 	 	 string `json:"name" binding:"required"`
	Surname 	 string `json:"surname" binding:"required"`
	Email 		 string `json:"email" binding:"required"`
	Project      string `json:"project" binding:"required"`
	Password 	 string `json:"password" binding:"required"`
	CreatedAt	 string `json:"-" db:"created_at"`
}

//Structure of request for Sign In method 
type SignInUserInput struct {
	Email 	 string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}