package main

import (
	"os"
	"send-mail-microservice/pkg/handler"
	"send-mail-microservice/pkg/repository"
	"send-mail-microservice/pkg/service"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	_ "github.com/lib/pq"
	sendmailmicroservice "send-mail-microservice"
)

// @title Send mail microservice
// @version 1.0
// @description API Send mail microservice

// @host localhost:8001
// @BasePath /

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	//Set logrus format
	logrus.SetFormatter(new(logrus.JSONFormatter))

	//Loade file with configs
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	//Load .env file with environment variables
	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variabels: %s", err)
	}

	//Create db connection
	db, err := repository.NewPostgresDB(repository.Config{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Username: os.Getenv("DB_USERNAME"),
		DBName:   os.Getenv("DB_NAME"),
		SSLMode:  os.Getenv("DB_SSL_MODE"),
		Password: os.Getenv("DB_PASSWORD"),
	})

	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())        
	}
	
	//Create(init) main repository structure
	repository := repository.NewRepository(db)

	//Create(init) main service structure
	service := service.NewService(repository)

	//Create(init) main handler structure
	handlers := handler.NewHandler(service)

	//Run app web server
	srv := new(sendmailmicroservice.Server)
	if err := srv.Run(os.Getenv("APP_PORT"), handlers.InitRoutes()); err != nil {
		logrus.Fatalf("error occurred while running http server: %s", err.Error())
	}

}

//Function that load configs from config/config.yml via viper library
func initConfig() error{
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}