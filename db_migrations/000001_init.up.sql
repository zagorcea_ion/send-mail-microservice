CREATE TABLE users
(
    id            serial not null unique,
    name          varchar(255) not null,
    surname       varchar(255) not null,
    email         varchar(255) not null unique,
    project       varchar(255) not null,
    password_hash varchar(255) not null,
    created_at    timestamptz not null default now()
);