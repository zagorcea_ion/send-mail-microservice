CREATE TABLE mails
(
    id            serial not null unique,
    subject       varchar(255) not null,
    content       text not null,
    user_id       int references users (id) on delete cascade not null,
    status        varchar(255) not null,
    emails        jsonb not null,
    created_at    timestamptz not null default now()
);