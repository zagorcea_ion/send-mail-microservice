package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
)

//Postgres database tables configuration
const (
	usersTable = "users"
	mailsTable = "mails"
)

//Define postgres configuration structure
type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

//Create new Postgres database connextion
//Params: Config structure
//Return: link to the sqlx.DB structure and potential errors
func NewPostgresDB(cfg Config) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode))
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return db, nil
}