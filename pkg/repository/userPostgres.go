package repository

import (
	"fmt"
	entity "send-mail-microservice/entity"
	"github.com/jmoiron/sqlx"
)

//UserPostgres repository structure
type UserPostgres struct {
	db *sqlx.DB
}

//UserPostgres repository constructor
//Params: db link to sqlx.DB structure - created db connection 
//Return: UserPostgres struct 
func NewUserPostgres(db *sqlx.DB) *UserPostgres {
	return &UserPostgres{db: db}
}

//Insert new user in repository
//Params: user as User entity
//Return: user id and potential errors 
func (r *UserPostgres) CreateUser(user entity.User) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (name, surname, email, project, password_hash) values ($1, $2, $3, $4, $5) RETURNING id", usersTable)

	row := r.db.QueryRow(query, user.Name, user.Surname, user.Email, user.Project, user.Password)
	
	if err :=row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

//Get (verifie) user by email and password
//Params: email, password string - user email and password has
//Return: User entity and potential errors
func (r *UserPostgres) GetUser(email, password string) (entity.User, error){
	var user entity.User

	query := fmt.Sprintf("SELECT id FROM %s WHERE email=$1 AND password_hash=$2", usersTable)
	err := r.db.Get(&user, query, email, password)

	return user, err
}