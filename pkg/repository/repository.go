package repository

import (
	"github.com/jmoiron/sqlx"
	entity "send-mail-microservice/entity"

)

//User repository interface 
type User interface {
	CreateUser(user entity.User) (int, error)
	GetUser(username, password string) (entity.User, error)
}

//Mail repository interface 
type Mail interface {
	Create(userId int, status string, mail entity.Mail) (int, error)
	GetById(userId, mailId int) (entity.Mail, error)
	GetAll(userId int) ([]entity.Mail, error)
}

//The main service structure 
type Repository struct {
	User
	Mail
}

//Repository constructor
func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		User: NewUserPostgres(db),
		Mail: NewMailPostgres(db),
	}
}