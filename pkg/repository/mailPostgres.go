package repository

import (
	"fmt"
	entity "send-mail-microservice/entity"

	"github.com/jmoiron/sqlx"
)

//MailPostgres repository structure
type MailPostgres struct {
	db *sqlx.DB
}

//MailPostgres repository constructor
//Params: db link to sqlx.DB structure - created db connection
//Return: MailPostgres struct
func NewMailPostgres(db *sqlx.DB) *MailPostgres {
	return &MailPostgres{db: db}
}

//Insert new mail in repository
//Params: user_id int, status string mail as Mail entity
//Return: mail id and potential errors
func (r *MailPostgres) Create(userId int, status string, mail entity.Mail) (int, error) {
	var id int

	//Prepare query
	query := fmt.Sprintf("INSERT INTO %s (subject, content, user_id, status, emails) values ($1, $2, $3, $4, $5) RETURNING id", mailsTable)

	//Set necessary data in query
	row := r.db.QueryRow(query, mail.Subject, mail.Content, userId, status, mail.Emails)

	//Extract record id from query
	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	return id, nil
}

//Get mail by id from repository
//Params: userId int, mailId int
//Return: entity.Mail and potential errors 
func (r *MailPostgres) GetById(userId, mailId int) (entity.Mail, error) {
	var mail entity.Mail

	//Prepare query
	query := fmt.Sprintf(`SELECT m.id, m.subject, m.content, m.user_id, m.status, m.emails, m.created_at  FROM %s m 
	INNER JOIN %s u on m.user_id = u.id WHERE m.id = $1 AND u.id = $2`, mailsTable, usersTable)

	//Set necessary data in query
	err := r.db.Get(&mail, query, mailId, userId)

	return mail, err
}

//Get all mails from repository
//Params: userId int
//Return: entity.Mail collection and potential errors
func (r *MailPostgres) GetAll(userId int) ([]entity.Mail, error) {
	var mails []entity.Mail

	//Prepare query
	query := fmt.Sprintf("SELECT * FROM %s WHERE user_id = $1", mailsTable)

	//Set necessary data in query
	err := r.db.Select(&mails, query, userId)

	return mails, err
}
