package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

//Custom error response structure
type errorResponse struct {
	Message string `json:"message`
}

//Custom error response 
func newErrorResponse(c *gin.Context, statusCode int, message string) {
	logrus.Error(message)
	c.AbortWithStatusJSON(statusCode, errorResponse{message})
}