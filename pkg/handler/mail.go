package handler

import (
	"net/http"
	entity "send-mail-microservice/entity"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Summary Send Mail
// @Security ApiKeyAuth
// @Tags mails
// @Description Send mail [Only: content, subject and emails fields is required]
// @ID Send-mail
// @Accept  json
// @Produce  json
// @Param input body entity.Mail true "Send mail data"
// @Success 200 {integer} integer 1
// @Failure 400,404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Failure default {object} errorResponse
// @Router /api/mails/send [post]
func (h *Handler) sendMail(c *gin.Context) {
	//Get user id
	userId, err := getUserId(c)
	if err != nil {
		return
	}

	//Convert request in Mail entity
	var input entity.Mail

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	//Call send mail services
	id, err := h.services.Mail.SendMails(userId, input)

	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

// @Summary Get all mails
// @Security ApiKeyAuth
// @Tags mails
// @Description Get all mails for logged user
// @ID Get-all-mail
// @Produce  json
// @Success 200 {object} entity.GetAllMailsResponse
// @Failure 400,404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Failure default {object} errorResponse
// @Router /api/mails/ [get]
func (h *Handler) getAllMails(c *gin.Context) {
	//Get user id
	userId, err := getUserId(c)
	if err != nil {
		return
	}

	mails, err := h.services.Mail.GetAll(userId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, entity.GetAllMailsResponse{
		Data: mails,
	})
}

// @Summary Get mail by ID
// @Security ApiKeyAuth
// @Tags mails
// @Description Get all mails for logged user
// @ID Get-mail-by-id
// @Produce  json
// @Param id path int true "Mail record ID"
// @Success 200 {object} entity.Mail
// @Failure 400,404 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Failure default {object} errorResponse
// @Router /api/mails/{id} [get]
func (h *Handler) getMailById(c *gin.Context) {
	//Get user id
	userId, err := getUserId(c)
	if err != nil {
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "invalid id param")
		return
	}

	mail, err := h.services.Mail.GetById(userId, id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, mail)
}
