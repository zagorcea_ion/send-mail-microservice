package handler

import (
	"send-mail-microservice/pkg/service"

	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/files"
	_ "send-mail-microservice/docs"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	auth := router.Group("/auth")
	{
		auth.POST("/sign-up", h.signUp)
		auth.POST("/sign-in", h.signIn)
	}

	api := router.Group("/api", h.checkUserAuth)
	{
		mails := api.Group("mails")
		{
			mails.POST("send", h.sendMail)
			mails.GET("/", h.getAllMails)
			mails.GET("/:id", h.getMailById)


		}
	}

	return router
}