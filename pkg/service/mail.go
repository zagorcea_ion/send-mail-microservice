package service

import (
	"log"
	"os"
	entity "send-mail-microservice/entity"
	"send-mail-microservice/pkg/repository"
	"strconv"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

//AuthService structure
type MailService struct {
	repo repository.Mail
}

//AuthService constructor
//Params: user repository
//Return: AuthService struct 
func NewMailService(repo repository.Mail) *MailService {
	return &MailService{repo: repo}
}

//Send mails
//Params: userId int, sendMail as Mail entity
//Return: id record and potential errors 
func (s *MailService) SendMails(userId int, sendMail entity.Mail) (int, error) {
	
    m := mail.NewV3Mail()	//create main mail object

	//Set data from and content 
    m.SetFrom(mail.NewEmail(os.Getenv("SEND_FROM_NAME"), os.Getenv("SEND_MAIL_FROM")))
    m.AddContent(mail.NewContent("text/html", sendMail.Content))
    
    // create new *Personalization
    personalization := mail.NewPersonalization()
    
	//Parse emails and add to the Personalization data
	for _, v := range sendMail.Emails {
		email := mail.NewEmail(v, v)
		personalization.AddTos(email)
	}

	//Set subject in Personalization data
    personalization.Subject = sendMail.Subject
  
    //Add Personalization data to main mail object
    m.AddPersonalizations(personalization)
  
	//Prepare and send SENDGRID API request
    request := sendgrid.GetRequest(os.Getenv("SENDGRID_API_KEY"), "/v3/mail/send", "https://api.sendgrid.com")
    request.Method = "POST"
    request.Body = mail.GetRequestBody(m)
    response, err := sendgrid.API(request)

    if err != nil {
        log.Println(err)

		return 0, err
    }

	//Store mail in repository
	status := strconv.Itoa(response.StatusCode)
		
	return s.repo.Create(userId, status, sendMail)
}

//Get mail by ID for logged user 
//Params: userId int, mailId int
//Return: entity.Mail and potential errors 
func (s *MailService) GetById(userId, mailId int) (entity.Mail, error) {
	return s.repo.GetById(userId, mailId)
}

//Get all mails for logged user 

func (s *MailService) GetAll(userId int) ([]entity.Mail, error) {
	return s.repo.GetAll(userId)
}
