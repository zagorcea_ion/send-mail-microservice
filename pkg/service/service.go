package service

import (
	"send-mail-microservice/pkg/repository"
	entity "send-mail-microservice/entity"
)

//Authorization service interface 
type Authorization interface {
	CreateUser(user entity.User) (int, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (int, error)


}

//Mail service interface 
type Mail interface {
	SendMails(userId int, sendMail entity.Mail) (int, error)
	GetById(userId, mailId int) (entity.Mail, error)
	GetAll(userId int) ([]entity.Mail, error)
}

//The main service structure 
type Service struct {
	Authorization
	Mail
}
//Service constructor
func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repos.User),
		Mail:		   NewMailService(repos.Mail),
	}
}
