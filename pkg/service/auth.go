package service

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"os"
	entity "send-mail-microservice/entity"
	"send-mail-microservice/pkg/repository"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	tokenTTL = 12 * time.Hour
)

//AuthService structure
type AuthService struct {
	repo repository.User
}

//Define custom structure for JWT claims
type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}

//AuthService constructor
//Params: user repository
//Return: AuthService struct 
func NewAuthService(repo repository.User) *AuthService {
	return &AuthService{repo: repo}
}

//Create user account
//Params: user as User entity
//Return: user id and potential errors 
func (s *AuthService) CreateUser(user entity.User) (int, error) {
	user.Password = generatePasswordHash(user.Password)
	
	return s.repo.CreateUser(user)
}

//Generate Auth Token 
//Params: email, password string [user email and password]
//Return: auth token and potential errors 
func (s *AuthService) GenerateToken(email, password string) (string, error) {

	user, err := s.repo.GetUser(email, generatePasswordHash(password))

	if err != nil {
		return "", err
	}

	token :=jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tokenTTL).Unix(),
			IssuedAt: time.Now().Unix(),
		},
		user.Id,
	})

	return token.SignedString([]byte(os.Getenv("JWT_SIGNING_KEY")))
}

//Parse token - verify if token is valid
//Params: accessToken string [user personal access token]
//Return: user id and potential errors 
func (s *AuthService) ParseToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}

		return []byte(os.Getenv("JWT_SIGNING_KEY")), nil
	})

	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, errors.New("token claims are not of type *tokenClaims")
	}

	return claims.UserId, nil
}

//Generate pasword hash with salt 
//Params: password string [user password]
//Return: password hash 
func generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(os.Getenv("PASSWORD_SALT"))))
}