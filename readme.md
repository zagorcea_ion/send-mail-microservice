# Requirements
- Golang installed on your PC
- Docker
- **golang-migrate** - package installed globally on your PC
    - [Setting up golang-migrate for windows](https://dev.to/prematid/setting-up-golang-migrate-for-windows-2nnb)
    - [Scoop - A command-line installer for Windows](https://scoop.sh/)
- **swaggo/swag** - installed on your PC 
    - [swaggo/swag](https://github.com/swaggo/swag)


# Installation guide
1. Run postgres service 
    - `docker pull postgres` - install postgres image
    - `docker run --name=send-mail -e POSTGRES_PASSWORD='qwerty' -p 5436:5432 -d --rm postgres` - run postgres container
2. Generate Swagger documentation - this step may be omitted
3. Create and configure `.env` file using `.env-example` file as an example
4. Run database migrations
5. Install necessary dependencies using the following command `go mod tidy`
6. Run app using the following command `go run cmd/main.go` 

## Guide how to use migration system

- `migrate create -ext sql -dir ./db_migrations -seq init` - command that create new migration
    - `init` - migration name
    - `./db_migrations` - directory path where migration is stored
- `migrate -path ./db_migrations -database 'postgres://postgres:qwerty@localhost:5436/postgres?sslmode=disable' up` - run migration 
    - `-path ./db_migrations` - directory path where migration is stored
    -  `-database 'postgres://postgres:qwerty@localhost:5436/postgres?sslmode=disable'` - database connection **[database configuration depend of your postgres service configuration]**
- `migrate -path ./db_migrations -database 'postgres://postgres:qwerty@localhost:5436/postgres?sslmode=disable' down` - rollback the migrations 

## Guide how to use swagger documentation generator
- Run the following command to generate API documentation `C:/go-path/go/bin/swag.exe init -g cmd/main.go`
    - `C:/go-path/go` - is your GOPATH
        - You may check your GOPATH using the following command `go env`
    - `-g cmd/main.go` - path to main.go file
- After command running in project you can find `docs` folder with documentation 
- Run the app and access the following link http://localhost:8001/swagger/index.html to view project documentation **[port depend of your env configurations]**  

**Note:** by default in repository is situated build of current documentation 